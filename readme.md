# Data Structures & Algorithms

Here, I intend to post python implementation of the common Data Structures and Algorithm concepts.

## Searching Techniques

The file ```./search.py``` has the following searching techniques implemented.

- Linear Search
- Binary Search
- Interpolation Search

## Sorting Techniques

The file ```./sort.py``` has the following searching techniques implemented.

- Bubble Sort
- Insertion Sort
- Selection Sort