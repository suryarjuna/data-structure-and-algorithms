"""
Python 3 implementation of search logic's for quick use.
Contains the following search logics:

1. Linear Search:
     In this type of search, a sequential search is made over all items one by one.
     Every item is checked and if a match is found then that particular item is returned,
     otherwise the search continues till the end of the data collection.

2. Binary Search:
    Binary search is a fast search algorithm with run-time complexity of Ο(log n).
    This search algorithm works on the principle of divide and conquer.
    For this algorithm to work properly, the data collection should be in the sorted form.

3. Interpolation Search:
    Interpolation Search is an improved variant of binary search.
    This search algorithm works on the probing position of the required value.
    For this algorithm to work properly, the data collection should be in a sorted form and equally distributed.
"""


def linear_search(search_list, search_object):
    """
    A Utility function to search and return the index of the location of an item
    :param search_list: The list of items in which the search has to be conducted.
    :param search_object: The item that is being searched for.
    :return: The flag to see if the item is present or not and the index if it exists.
    """
    flag = False
    index = None
    message = ""

    for i in range(len(search_list)):
        print("Iteration: ", i)
        if search_list[i] == search_object:
            flag = True
            index = i
            break
        else:
            pass
    return flag, index, message


def binary_search(search_list, search_object):
    """
    A Utility function to search and return the index of the location of an item
    :param search_list: The list of items in which the search has to be conducted.
    :param search_object: The item that is being searched for.
    :return: The flag to see if the item is present or not and the index if it exists.
    """
    flag = False
    index = None
    message = ""

    lower_bound = 0
    upper_bound = len(search_list)
    counter = 0

    while True:
        counter = counter + 1
        print("Iteration: ", counter)
        if upper_bound < lower_bound:
            flag = False
            index = None
            message = "Value does not exist."
            break

        mid_point = int(lower_bound + ((upper_bound - lower_bound) / 2))

        if search_list[mid_point] < search_object:
            lower_bound = mid_point + 1

        elif search_list[mid_point] > search_object:
            upper_bound = mid_point - 1

        elif search_list[mid_point] == search_object:
            flag = True
            index = mid_point
            message = "Value found!"
            break

    return flag, index, message


def interpolation_search(search_list, search_object):
    """
    A Utility function to search and return the index of the location of an item
    :param search_list: The list of items in which the search has to be conducted.
    :param search_object: The item that is being searched for.
    :return: The flag to see if the item is present or not and the index if it exists.
    """
    flag = False
    index = None
    message = ""

    lower_bound = 0
    upper_bound = len(search_list) - 1
    counter = 0

    while True:
        counter = counter + 1
        print("Iteration: ", counter)
        if lower_bound == upper_bound or search_list[lower_bound] == search_list[upper_bound]:
            flag = False
            index = None
            message = "Value not found"
            break

        mid_point = int(lower_bound + (
                (upper_bound - lower_bound) / (search_list[upper_bound] - search_list[lower_bound]) * (
                search_object - search_list[lower_bound])))
        print(mid_point, search_list[mid_point])
        if search_list[mid_point] == search_object:
            flag = True
            index = mid_point
            message = "Value Found"
            break

        else:
            if search_list[mid_point] < search_object:
                lower_bound = mid_point + 1
            elif search_list[mid_point] > search_object:
                upper_bound = mid_point - 1
    return flag, index, message


if __name__ == '__main__':
    data = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]

    print("Linear Search")
    print(linear_search(search_list=data, search_object=6))
    print("Binary Search")
    print(binary_search(search_list=data, search_object=6))
    print("Interpolation Search")
    print(interpolation_search(search_list=data, search_object=6))
