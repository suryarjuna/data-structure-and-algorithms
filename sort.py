"""
Python implementation of sorting techniques for quick use.

1. Bubble Sort:
    Bubble sort is a simple sorting algorithm. This sorting algorithm is comparison-based algorithm in which
    each pair of adjacent elements is compared and the elements are swapped if they are not in order.
    This algorithm is not suitable for large data sets as its average and worst case complexity are of Ο(n2)
    where n is the number of items.

2. Insertion Sort:
    This is an in-place comparison-based sorting algorithm.
    Here, a sub-list is maintained which is always sorted.
    For example, the lower part of an array is maintained to be sorted.
    An element which is to be 'insert'ed in this sorted sub-list, has to find its appropriate place and
    then it has to be inserted there. Hence the name, insertion sort.

3. Selection Sort:
    Selection sort is a simple sorting algorithm.
    This sorting algorithm is an in-place comparison-based algorithm in which the list is divided into two parts,
    the sorted part at the left end and the unsorted part at the right end.
    Initially, the sorted part is empty and the unsorted part is the entire list.
"""


def bubble_sort(sort_list):
    """
    A function that returns sorted list
    :param sort_list: An unsorted list of items
    :return: A sorted list
    """
    for j in range(len(sort_list)):
        for i in range(len(sort_list)):
            if (i + 1) < len(sort_list):
                if sort_list[i] > sort_list[i + 1]:
                    sort_list[i], sort_list[i + 1] = sort_list[i + 1], sort_list[i]

    return sort_list


def insertion_sort(sort_list):
    """
       A function that returns sorted list
       :param sort_list: An unsorted list of items
       :return: A sorted list
    """

    for i in range(1, len(sort_list)):
        current_position = i
        current_value = sort_list[i]

        while current_position > 0 and sort_list[current_position - 1] > current_value:
            sort_list[current_position] = sort_list[current_position - 1]
            current_position = current_position - 1

        sort_list[current_position] = current_value
    return sort_list


def selection_sort(sort_list):
    """
       A function that returns sorted list
       :param sort_list: An unsorted list of items
       :return: A sorted list
    """

    for i in range(0, len(sort_list)):
        minimum_value = sort_list[i]
        minimum_index = i
        for j in range(i + 1, len(sort_list)):
            if sort_list[j] < minimum_value:
                minimum_value = sort_list[j]
                minimum_index = j
        sort_list[i], sort_list[minimum_index] = sort_list[minimum_index], sort_list[i]
    return sort_list


if __name__ == '__main__':
    data = [14, 33, 27, 35, 10]
    print(selection_sort(data))
